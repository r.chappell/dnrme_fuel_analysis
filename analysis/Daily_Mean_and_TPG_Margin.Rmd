---
title: "Daily_mean_p_deviations_and_TPG_margin_analysis_Oct_2020"
author: "Rhetta Chappell - Regional Innovation Data Lab"
date: "20/10/2020"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE,knitr::opts_chunk$set(fig.height=5, fig.width=11, out.width="\\textwidth", comment = ""))
```

```{r, echo = FALSE, warning=FALSE}
#load packages
suppressPackageStartupMessages({
  extrafont::loadfonts(device="win", quiet = TRUE)#fonts for themes
  #data
  #~~~~~~~~~~~~~~~~~~~~
  library(tidyverse);
  library(reshape2); 
  library(purrr);
  library(data.table)
   library(clustMixType) #kproto algorithm optimised for mixed data types which is what we have
  #article for above package https://journal.r-project.org/archive/2018/RJ-2018-048/RJ-2018-048.pdf
  library(factoextra)
  library(NbClust) #determining the right number of k clusters for the kmeans,mode and or prototype algorithm
  
  #shape files and maps
  #~~~~~~~~~~~~~~~~~~~~
  library(rgdal); 
  library(raster) # to clip/crop the Brisbane LGA shape file
  library(maps)
  
  #Visualisation and animation
  #~~~~~~~~~~~~~~~~~~~~
  library(gganimate)
  library(ggrepel)
  library(av) #to access the av_renderer for output in mp4/video format
  library(Cairo) #to improve smoothness and detail in animation
  library(gifski)
  library(tweenr)
  library(transformr)
  library(wesanderson)
  library(hrbrthemes) #theme for animation
  #library(ggthemes) #themes
  #library(viridis);

})

untibble <- function (tibble) {  ## from WWRCourse
  data.frame(unclass(tibble), check.names = FALSE, stringsAsFactors = FALSE)
}

#Aesthetics - colour scheme
cols <- c("#011638", "#aa9aba","#e26177", "#e51212", "#f9a03f" )
pal <-colorRampPalette(c("#011638", "#aa9aba","#e26177", "#e51212", "#f9a03f" ))
theme_set(theme_bw() + theme(plot.title = element_text(hjust = 0.5)))

oldOpt <- options(warn = -1)
```


```{r, echo = FALSE}


#############################NOT RUN################################################

#  data <- as.data.frame(fread("data/master_data/masterdata05_2020.csv"))
#  
#do some observation and variable reduction, tidying and sorting
#  data_2 <- data %>%
#   rename(Time = TransactionDateutc,
#          LGA_NAME16 = `LGA Name`,
#          SA2_NAME16 = `SA2 Name`) %>%
#   mutate(Time = as.POSIXct(Time,format = "%Y-%m-%d %H:%M")) %>%
#    #FOR REPORT CUT HERE - JULY 2020
#   #only want records from 1/06/2019 onwards as the policy became mandatory in May 2019
#   #filter(Time > as.POSIXct("2019-05-31 00:00:00")) %>%
#    #FOR RESEARCH CUT HERE - OCT 2020
#   #only want records from 1/06/2018 onwards as the policy became mandatory in May 2019 check what was happening 1 yr before policy
#     filter(Time > as.POSIXct("2018-05-31 00:00:00")) %>%
#   within({
#     Seconds <- as.numeric(Time)
#   }) %>%
#   dplyr::select(-V1) %>%
#   arrange(SiteId, Seconds) %>%
#  #filter to only keep unleaded and diesel
#   filter(`Fuel Type` %in% c("diesel", "unleaded"))
# # 
# write.csv(data_2, file = "data/Fuel_master_subset_06_2018_03_2020_Qld_un_die.csv")

```
###Read in the subset above and continue with analysis. 
Using the subset to save time and make it easier to knit this document, as master dataset contains: 5,758,916 obs, by subsetting to 1 year prior to policy and only keeping records for unleaded and diesel fuel types, we're reducing the subset to 1,594,220 obs. We may further subset to only LGAs in SE Qld and focus on unleaded fuel.
```{r, echo=TRUE}
#read in data subset with all fuel data
data_18_20_qld <- as.data.frame(fread('data/Fuel_master_subset_06_2018_03_2020_Qld_un_die.csv', stringsAsFactors = TRUE))
#check out the data structure
str(data_18_20_qld)

#set date time format and keep only unleaded and diesel
data_18_20_qld_unleaded <- data_18_20_qld %>% 
  #format date/time variable
  mutate(Time = as.POSIXct(Time,format = "%Y-%m-%d %H:%M")) %>% 
  mutate(Date = as.POSIXct(Date,format = "%Y-%m-%d %H:%M")) %>% 
  dplyr::select(-V1) %>% 
  #only keep unleaded for now - 383,135 obs
   filter(`Fuel Type` == "unleaded")

#the master dataset doesn't contain the site Lat and Longs _ we gathered these with the Google API key - to avoid paying for all of them again we will just merge on the lats and longs which we have these are only for SE Qld.


SE_qld_lat_long <- read.csv("data/SEQLDUn.csv")
SE_qld_lat_long <- SE_qld_lat_long %>% 
  dplyr::select(SiteId,Site.Latitude, Site.Longitude) %>% 
  arrange(SiteId) %>% 
  distinct(SiteId, .keep_all = TRUE)

#merge onto df above
data_18_20_qld_unleaded <- merge(data_18_20_qld_unleaded, SE_qld_lat_long, by = "SiteId", all.x = TRUE)

```
###Read in the list of ABNs and SiteIds - list from Tom

We want to check out market concentration - not only by brand but by ABN as not all branded stations are owned by the same company but there is a high degree of concentration. Usually an ABN owns one brand of station, but some own a few.

```{r, echo=TRUE}
#read in the ABN and siteId csv and merge on SiteId - this way we can calculate market share by brand and ABN
abn_site <- read.csv("data/abn2.csv", stringsAsFactors = TRUE)
str(abn_site)
#tidy up column names
colnames(abn_site)[1] <- "SiteId"

#merge
data_abn_m <- merge(abn_site, data_18_20_qld_unleaded, by = "SiteId", all.y = TRUE)
str(data_abn_m)
#remove spaces in ABN column
data_abn_m <- data_abn_m %>% 
  mutate(Seller_ABN = as.factor(gsub(" ", "", Seller.ABN))) %>% 
  dplyr::select(-Seller.ABN)

#now we want only one record per ABN and SiteId so that we can do counts around the following:
# sites per ABN
# sites per Brand
# sites per LGA
# sites per Sa2
# sites per postcode

data_abn_counts <- data_abn_m %>% 
  #get the columns of interest
  dplyr::select(SiteId, `Site Brand`, `Site Post Code`, SA2_NAME16, LGA_NAME16, Seller_ABN) %>%
  unique() %>% 
  #brand counts all qld
  add_count(`Site Brand`) %>% 
  rename(Brand_Count_Qld = n) %>% 
  #ABN counts all qld
  add_count(Seller_ABN) %>% 
  rename(ABN_Count_Qld = n) %>% 
  #ABN counts per LGA
  add_count(Seller_ABN,LGA_NAME16) %>% 
  rename(ABN_Count_LGA = n) %>% 
  #ABN counts per SA2
  add_count(Seller_ABN,SA2_NAME16) %>% 
  rename(ABN_Count_SA2 = n) %>% 
  #ABN counts per postcode
  add_count(Seller_ABN, `Site Post Code`) %>% 
  rename(ABN_Count_PC = n) 

#merge back onto the full data set with the daily price data
data_abn_m2 <- merge(data_abn_m, data_abn_counts, by = c("SiteId", "Site Brand", "Site Post Code", "SA2_NAME16", "LGA_NAME16", "Seller_ABN"))
```

###AIP- Oct 2020 Tail Gate Price - TGP daily and annual
Merge on the TGP to the main daily fuel price dataset we need these variable to calculate the margins between selling price and the TGP.

Hypothesis to test: By coordinating higher margins at the top of the cycle (spike) firms are raising margins over the course of the cycle but also overall overtime. We want to see whether over time the margin is higher due to the cycles. 
```{r, echo=TRUE}
#now we need to read in the daily and annual TGP for Brisbane and merge with our data set
#DAILY tgp - extracted Oct 2020: https://aip.com.au/historical-ulp-and-diesel-tgp-data
aip_tgp <- read.csv("data/AIP_TGP_Data_09-Oct-2020.csv")
#str(aip_tgp)

aip_tgp_bris <- aip_tgp %>% 
  dplyr::select(AVERAGE.ULP.TGPS..inclusive.of.GST., Brisbane, National.Average) %>% 
  rename(Date = AVERAGE.ULP.TGPS..inclusive.of.GST.,
         Brisbane_Daily_TGP = Brisbane,
         National_Average_Daily_TGP = National.Average) %>% 
  #replace / with - for merging with existing Time column above 
  mutate(Date = (gsub("/", "-", Date))) %>% 
  #format date/time variable
  mutate(Date = as.POSIXct(Date,format = "%m-%d-%Y")) %>% 
  #filter to keep data between June 2018 and March 2020 same as fuel data
  filter(Date > as.POSIXct("2018-05-31")) 

#str(aip_tgp_bris)
#merge on the date column so now we know the daily TGP per
data_abn_m3 <- merge(data_abn_m2, aip_tgp_bris, by = "Date", all.x = TRUE)

#check the NAs as we will need to back fill TGP from previous day
#there are a lot so we will need to fill
data_abn_m3_NA_fill <- data_abn_m3 %>%
  arrange(SiteId,Time) %>% 
  #group_by(SiteId) %>% 
  #default fill direction in down
  fill(Brisbane_Daily_TGP,National_Average_Daily_TGP ) %>% 
  #still misses 17 obs so we will fill upwards as well
  fill(Brisbane_Daily_TGP,National_Average_Daily_TGP, .direction = "up")

#check for nas - we want 0
na_check <- data_abn_m3_NA_fill %>% 
  filter(is.na(Brisbane_Daily_TGP))

```

###Time to calculate the change in price per station
By visualising the changes in price we can see that the spikes in the cycle start to increase and more stations are taking part after the policy was implemented in May 2019. The mechanism to report was in place 1 year prior to the implementation of mandatory reporting - so during this lead up year we should see some signalling among leaders to try and coordinate the cycles.
 
 
```{r,echo=TRUE}

str(data_abn_m3_NA_fill)
Qld_change_price <- data_abn_m3_NA_fill%>% 
  #dplyr::select(everything()) %>% 
  #Select columns of interest
  dplyr::select(SiteId, Site.Longitude, Site.Latitude, Price, Time, Seconds, Seller_ABN, ABN_Count_Qld, `Site Brand`, LGA_NAME16, Brisbane_Daily_TGP, `Site Post Code`, Date) %>%
  #Sort rows by Time and SiteId
  arrange(Time, SiteId) %>% untibble() %>% 
  #split the dataframe by row (observation) into sep lists one per SiteId
  #the . means split is to be applied to all 
  split(., .$SiteId) %>% 
  # for each of the lists (1 per SiteId) create a column called Jump 
  #Jump will show us the difference in price from one price change (date/time) to the next
  lapply(function(Id) within(Id, Change_in_price <- c(0, diff(Price)))) %>% 
  #bind all the lists row by row (as per the columns selected above) one row per SiteId
  do.call(rbind, .) 

#check
#str(Qld_change_price)

#visualise
ggplot(Qld_change_price) + aes(x = Time, y = Change_in_price, colour = Change_in_price, group = seq_along(rev(Change_in_price))) + 
  geom_point(size = 0.75) + scale_colour_gradientn (colours = cols)+
  #place a y-intercept line at our cutoff point of 0.15
  geom_hline(yintercept = 0.15, colour = "blue")+
  #Line when policy started
   geom_vline(aes(xintercept = as.integer(as.POSIXct("2019-05-01"))), col = "red")+
  labs(title = "Identifying cut off points to observe price reset cycles", 
       subtitle = "Unleaded Fuel Queensland: June 2018 to April 2020",
       caption = "Regional Innovation Data Lab",
       y= "Price change (AUD$)", x = "")
```

###Daily median price deviations

Convert prices to daily mean price deviations for each market
- calculate mean price per postcode and divide it by the mean price for that capital city or LGA (or could do Brisbane and rest of Qld)
- create a box and whiskier plot pre and post policy and see whether the skewness changes (in WA post policy plots are R skewed meaning the policy eliminated the lower end of the prices range)


###Cumulative distribution of postcode ranks
 - Assign each postcode a daily rank from low to high in regards to the mean price
 - these are then averaged across the postcodes
 - order/rank the postcodes from lowest average to highest average price and then plot the cumulative averages
 

```{r, echo = TRUE}


PC_mean_price_dev <- Qld_change_price %>% 
  group_by(`Site Post Code`, Date) %>% 
  summarise(PC_mean_daily_p = mean(Price),
            PC_SD_daily_p = sd(Price),
            PC_median_daily_p = median(Price)) %>% 
  ungroup() 

LGA_mean_price_dev <- Qld_change_price %>%
  filter(LGA_NAME16 == "Brisbane (C)") %>% 
  group_by(LGA_NAME16, Date) %>% 
  summarise(LGA_mean_daily_p = mean(Price),
            LGA_SD_daily_p = sd(Price),
            LGA_median_daily_p = median(Price)) %>% 
  ungroup() %>% 
  dplyr::select(- LGA_NAME16)

PC_LGA_means<-merge(PC_mean_price_dev, LGA_mean_price_dev, by = "Date")


PC_LGA_means_2<- PC_LGA_means %>% 
  group_by(Date,`Site Post Code`) %>% 
  mutate(Daily_mean_price_dev = PC_mean_daily_p/LGA_mean_daily_p)

```




###Now we are going to define what a cycle or surge is.
Previously we had defined it as a change in price of more than +.015 AUD. In the literature from WA they define it has a change in price of more than +0.06

```{r, echo = TRUE}

#Cycle based on a price increase of more than $0.15
Qld_change_price_cycle_15 <- Qld_change_price %>% 
  dplyr::select(everything()) %>% 
  filter(Change_in_price > 0.15) %>% #only keep obs with 0.15 price diff
  arrange(Time) %>% #sort rows by time
  within({
    # calculate the gap/lag
    Gap <- c(0, diff(Seconds))/(60*60*24) 
  }) %>% 
  untibble()

#visualise
ggplot(Qld_change_price_cycle_15) + aes(x = Time, y = Change_in_price, colour = Change_in_price) + 
  geom_point(size = 0.75) + scale_colour_gradientn(colours = cols[3:5]) +
  geom_hline(yintercept = 0.15, colour = "red")+
  ggtitle("Stations with a price change of + $0.15 or more")


#Cycle based on a price increase of more than $0.06
Qld_change_price_cycle_06 <- Qld_change_price %>% 
  dplyr::select(everything()) %>% 
  filter(Change_in_price > 0.06) %>% #only keep obs with 0.15 price diff
  arrange(Time) %>% #sort rows by time
  within({
    # calculate the gap/lag
    Gap <- c(0, diff(Seconds))/(60*60*24) 
  }) %>% 
  untibble()

#visualise
ggplot(Qld_change_price_cycle_06) + aes(x = Time, y = Change_in_price, colour = Change_in_price) + 
  geom_point(size = 0.75) + scale_colour_gradientn(colours = cols[3:5]) +
  geom_hline(yintercept = 0.15, colour = "red")+
  geom_hline(yintercept = 0.06, colour = "blue")+
  ggtitle("Stations with a price change of + $0.06 or more")

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# now we want to let the siteID of stations that don't surge
#sites that surge based on our 0.15 and 0.06 definitions
surge_sites_06 <- unique(as.factor(Qld_change_price_cycle_06$SiteId))
surge_sites_15 <- unique(as.factor(Qld_change_price_cycle_15$SiteId))
#all sites - there 1224 unique siteids in our dataset
unique_sites <- unique(as.factor(data_abn_m3$SiteId))

#39% or 481 out of 1224 don't surge at the 0.15 definition
not_surging_15 <- unique_sites[!(unique_sites %in% surge_sites_15)]
not_surge_df_15 <- as.data.frame(not_surging_15)

#7% or 87 out of 1224 don't surge based on 0.06 definition
not_surging_06 <- unique_sites[!(unique_sites %in% surge_sites_06)]
not_surge_df_06 <- as.data.frame(not_surging_06)

```

###Define the surge cycles after the implementation of the policy and identify the leaders. Create a scoring/ranking mechanism to order the sites within each surge/cycle
```{r, echoo = TRUE}
#to identify the leaders we will focus on the records after the policy is in place
Qld_change_price_cycle_15_2 <- Qld_change_price_cycle_15 %>% 
 filter(Time < as.POSIXct("2020-03-20 00:00:00")) %>% 
  filter(Time >  as.POSIXct("2019-06-01 00:00:00"))

times <- with(Qld_change_price_cycle_15_2, {
  #new variable to store the Gap variable sorted in decreasing order within each of the 12 sugres/intervals [1:13] 
  top_gaps <- sort(Gap, decreasing = TRUE)[1:12] ## visually 12 gaps - although we see pattern change in March due to codi-19
  #store a subset of the price change Times for the 10  dec sorted gaps
  times <- Time[Gap %in% top_gaps]
  sort(times)[1:12]  #sort
})


#visualise
ggplot(Qld_change_price_cycle_15_2) + aes(x = Time, y = Change_in_price, colour = Change_in_price, group = seq_along(Time)) + 
  geom_point() + scale_colour_gradientn(colours = cols[2:5]) +
  #place a y-intercept line at our cutoff point of 0.15
  geom_hline(yintercept = 0.15, colour = "blue")+
  #lines to define surges/cycles
  geom_vline(xintercept = times, colour = "blue")+
  labs(title = "Identifying cut off points to observe price reset cycles", 
       subtitle = "Unleaded Fuel SE Queensland: June 2019 to March 2020",
       caption = "Regional Innovation Data Lab",
       y= "Price change (AUD$)", x = "")



Qld_cycle_15_score <- Qld_change_price_cycle_15_2 %>% 
  dplyr::select(everything()) %>% 
  within({
    # find the price change Times that fall within the  times object created above - the 11 intervals/surges 
    #findInterval searches in a vector of ranges for the interval that a given value belongs to
    surge <- findInterval(Time, times) 
    #sequential ranks - rank each site within each surge
    rank <- ave(Seconds, surge, FUN = base::rank)
    #average relative rank 0- start of surge, 1 = last 
    score <- rank/ave(Seconds, surge, FUN = base::length)
    #set ABN to factor
    Seller_ABN <- as.factor(Seller_ABN)
    #calculate the average lag time across all sites and surges
    Average_lag_all_surges <- mean(Gap)
    #mean price
    Average_price_change <- mean(Change_in_price)
  })
```
###Produce a table to analyse how frequently sites participate in positive price surges
Looking at the table we can quickly see that a number of sites participate in all 10 surges and in each surge hike their price by > 0.15c or more with the Jump column showing the sites mean price increase over > 0.15c

```{r, echoo = TRUE}

###create a new table with 5 columns
# No: No of times that site had a price increase > 15c        
# Surges: No of times that site took part in a price surge, as defined 
# Change_in_price: mean price increase when over 15c 
# Rank ABNs based on their mean time per surge - rank
# Score: average relative score - A measure of how quickly they increased their price after the start of a surge 
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  

no <- with(Qld_cycle_15_score, table(SiteId)) %>% 
  as.data.frame(responseName = "No", stringsAsFactors = FALSE)

sc <- with(Qld_cycle_15_score, tapply(score, SiteId, mean)) %>% 
  data.frame(SiteId = names(.), Score = ., stringsAsFactors = FALSE)

jp <- with(Qld_cycle_15_score, tapply(Change_in_price, SiteId, mean)) %>% 
  data.frame(SiteId = names(.), Change_in_price = ., stringsAsFactors = FALSE)

pr <- with(Qld_cycle_15_score, table(SiteId, surge)) 

pr <- rowSums(pr > 0) %>% 
  data.frame(SiteId = names(.), Surges = ., stringsAsFactors = FALSE)
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Result <- no %>% 
  merge(pr, by = "SiteId") %>% 
  merge(jp, by = "SiteId") %>%
  merge(sc, by = "SiteId") %>% 
  arrange(Score) %>% untibble()
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#show the head and tail of the table
rbind(head(Result, 15), NA, tail(Result))


```

